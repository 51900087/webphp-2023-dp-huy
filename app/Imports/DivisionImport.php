<?php

namespace App\Imports;

use App\Models\Division;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
class DivisionImport implements WithCalculatedFormulas, WithValidation, ToModel, WithHeadingRow, SkipsOnFailure
{
    use Importable, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        $date = date('Y-m-d H:i:s');
        if($row['delete'] != NULL){
            if(strpos($row['delete'], '“') !== false || strpos($row['delete'], '”') !== false){
                $delete = str_replace('“', "'", str_replace('”', "'", $row['delete']));
                $delete = trim($delete, "'");
            } else {
                $delete = $row['delete'];
            }
            if($delete == 'Y'){
                $row['delete'] = $date;
            }else{
                $row['delete'] = NULL;
            }
        }
        if($row['id'] != NULL) {
            $division = Division::where('id',$row['id'])->first();
            if($division != NULL){
                $division->name = $row['division_name'];
                if($row['division_note'] != NULL){
                    $division->note = $row['division_note'];
                }
                $division->division_leader_id = $row['division_leader'];
                $division->division_floor_num = $row['floor_number'];
                $division->updated_date = $date;    
                if($row['delete'] != NULL){
                    $division->deleted_date = $row['delete'];
                }
                $division->save();
            }
        }else{
            if ($row['division_note'] == NULL) {
                $row['division_note'] = '';
            } else {
                $row['division_note'] = $row['division_note'];
            }
            return new Division([
                'name' => $row['division_name'],
                'note' => $row['division_note'],
                'division_leader_id' => $row['division_leader'],
                'division_floor_num' => $row['floor_number'],
                'created_date' => $date,
                'updated_date' => $date,
                'deleted_date' => $row['delete'],
            ]);
        }
    }
        
    public function rules(): array
    {
        return [
            'id' => 'nullable|integer|exists:division,id',
            'division_name' => 'required|string',
            'division_note' => 'nullable|string',
            'division_leader' => 'required|integer',
            'floor_number' => 'required|integer',
            'delete' => 'nullable',
        ];
    }
    public function customValidationAttributes()
    {
        return [
            'id' => config('constant.import.division.id.name'),
            'division_name' => config('constant.import.division.division_name.name'),
            'division_note' => config('constant.import.division.division_note.name'),
            'division_leader' => config('constant.import.division.division_leader_id.name'),
            'floor_number' => config('constant.import.division.floor_number.name'),
            'delete' => config('constant.import.division.delete.name'),
        ];
    }

    public function customValidationMessages()
    {
        $messagesECL001 = config('constant.messages.error.ECL001');
        $messagesECL010 = config('constant.messages.error.ECL010');
        $messagesECL094 = config('constant.messages.error.ECL094');
        return [
            'id.integer' => config('constant.import.division.id.name'). $messagesECL010,
            'id.exists' => config('constant.import.division.id.name') . $messagesECL094,
            'division_name.required' => config('constant.import.division.division_name.name'). $messagesECL001,
            'division_name.string' => config('constant.import.division.division_name.name'). $messagesECL010,
            'division_note.string' => config('constant.import.division.division_note.name') . $messagesECL010,
            'division_leader.required' => config('constant.import.division.division_leader_id.name'). $messagesECL001,
            'division_leader.integer' => config('constant.import.division.division_leader_id.name'). $messagesECL010,
            'floor_number.required' => config('constant.import.division.floor_number.name') . $messagesECL001,
            'floor_number.integer' => config('constant.import.division.floor_number.name'). $messagesECL010,
        ];
    }

}
