$(document).ready(function () {
    $("#file").change(function () {
        var fileExtension = ['csv'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            //append error message for id = error
            $("#error").html("Only '.csv' formats are allowed.");
            $(this).val('');
        }
    });
});