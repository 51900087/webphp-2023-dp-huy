<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'email',
        'password',
        'name',
        'division_id',
        'entered_date',
        'position_id',
        'created_date',
        'updated_date',
        'deleted_date',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUserByEmail($email)
    {
        return $this->where('email', $email)->whereNull('deleted_date')->first();
    }

    public function getUserByIDWithoutDelete($id)
    {
        return $this->where('id', $id)->whereNull('deleted_date')->first();
    }
    public function getUserById($id)
    {
        return $this->where('id', $id)->first();
    }

    public function division()
    {
        return $this->belongsTo(Division::class, 'division_id', 'id');
    }

    public function search($keyword,$type) {
        $this->getConnection()->reconnect();
        $result =  User::Query();
        if($keyword['username'] != null){
            $result = $result->where('name', 'like', '%'.$keyword['username'].'%');
        } 
        if($keyword['enteredDateFrom'] != null && $keyword['enteredDateTo'] != null){
            $result = $result->whereBetween('entered_date', [$keyword['enteredDateFrom'], $keyword['enteredDateTo']]);
        }
        if($keyword['enteredDateFrom'] != null && $keyword['enteredDateTo'] == null){
            $result = $result->where('entered_date', '>=', $keyword['enteredDateFrom']);
        }
        if($keyword['enteredDateFrom'] == null && $keyword['enteredDateTo'] != null){
            $result = $result->where('entered_date', '<=', $keyword['enteredDateTo']);
        }
        $result = $result->whereNull('deleted_date')->orderBy('name', 'ASC')->orderBy('id', 'ASC');
        $result = $result->selectRaw('id, name, email, division_id, DATE_FORMAT(entered_date, "%d/%m/%Y") as entered_date, position_id, DATE_FORMAT(created_date, "%d/%m/%Y") as created_date, DATE_FORMAT(updated_date, "%d/%m/%Y") as updated_date');
        if($type == 'csv') {
            return $result->get();
        }
        return $result->paginate(10);
    }

    public function remove($id)
    {
        $user = $this->getUserById($id);
        $user->deleted_date = date('Y-m-d H:i:s');
        $user->save();
    }

    public function add($data)
    {
        $user = new User();
        $check_email = $this->getUserByEmail($data['email']);
        if($check_email){
            return false;
        } else {
            $user->email = $data['email'];
            $user->password = Hash::make($data['password']);
            $user->name = $data['username'];
            $user->division_id = $data['division'];
            $user->entered_date = $data['entered_date'];
            $user->position_id = $data['position'];
            $user->created_date = date('Y-m-d H:i:s');
            $user->updated_date = date('Y-m-d H:i:s');
            $user->save();
            return true;
        }
    }

    public function updateUser($data,$id) {
        $user = new User();
        $user = $user->getUserByIDWithoutDelete($id);
        if($user){
            if(isset($data['email']) || isset($data['username']) || isset($data['entered_date']) || isset($data['position'])){
                $data['position'] = $data['position'][0];
                $data['division'] = $data['division_hidden'];
                $data['position'] = (int)$data['position'];
                $data['entered_date'] = date('Y-m-d', strtotime($data['entered_date']));
                if($user->password == $data['password']){
                    $user->email = $data['email'];
                    $user->name = $data['username'];
                    if($data['division']!= null){
                        $user->division_id = $data['division'];
                    }
                    $user->entered_date = $data['entered_date'];
                    $user->position_id = $data['position'];
                    $user->updated_date = date('Y-m-d H:i:s');
                } else {
                    if($data['password'] != null){
                        $user->password = Hash::make($data['password']);
                    }
                    $user->email = $data['email'];
                    $user->name = $data['username'];
                    $user->division_id = $data['division'];
                    $user->entered_date = $data['entered_date'];
                    $user->position_id = $data['position'];
                    $user->updated_date = date('Y-m-d H:i:s');
                }
            } else {
                if($data['password'] != null){
                    $password_hashed = Hash::make($data['password']);
                    if($user->password != $password_hashed){
                        $user->password = $password_hashed;
                        $user->updated_date = date('Y-m-d H:i:s');
                    }
                }
            }
            $user->save();
            return $user;
        }
    }

    public function getUsernameByDivisionLeaderID($id)
    {
        return $this->where('id', $id)->whereNull('deleted_date')->first();
    }

    public function checkEmailInSystemUpdate($id,$email){
        $user = $this->getUserByIDWithoutDelete($id);
        if($user->email == $email){
            return true;
        } else {
            $check_email = $this->getUserByEmail($email);
            if($check_email){
                return false;
            } else {
                return true;
            }
        }
    }
}
