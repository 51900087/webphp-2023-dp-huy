<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login()
    {
        if (Auth::check()) {
            return redirect()->intended('/');
        } else {
            return view('auth.login');
        }
    }

    public function checkLogin(Request $request)
    {
        $data = $request->only('email', 'password');
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if (Auth::check()) {
            $positionData = config('constant.positions');
            return redirect()->intended('/')->with('positionData', $positionData);
        } else {
            $userModel = new User();
            $user = $userModel->getUserByEmail($data['email']);
            if ($user) {
                if (Hash::check($data['password'], $user->password)) {
                    Auth::login($user);
                    Session()->put('user', $user);
                    $positionData = config('constant.positions');
                    return redirect()->intended('/')->with('positionData', $positionData);
                } else {
                    $messageECL016 = config('constant.messages.error.ECL016');
                    Session()->flash('error', $messageECL016);
                    return redirect()->route('login.get');
                }
            }
            $messageECL016 = config('constant.messages.error.ECL016');
            Session()->flash('error', $messageECL016);
            return redirect()->back();
        }
    }

    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->invalidate();
        return redirect('/')->with('message', 'Successfully logged out');
    }
}
