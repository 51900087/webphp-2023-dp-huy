<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class DivisionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user_id = \App\Models\User::pluck('id')->toArray();
        $division_name = ['Admin', 'Manager', 'Staff', 'Leader', 'Member', 'Guest'];
        $note = ['Manager', 'Translator', 'Developer', 'Leader'];
        return [
            'name' => $this->faker->randomElement($division_name),
            'note' => $this->faker->randomElement($note),
            'division_leader_id' => $this->faker->randomElement($user_id),
            'division_floor_num' => $this->faker->numberBetween(1, 10),
            'created_date' => $this->faker->dateTime(),
            'updated_date' => $this->faker->dateTime(),
            'deleted_date' => null,
        ];
    }   
}
