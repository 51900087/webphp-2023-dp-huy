$('#login-form').validate({
    onfocusout: function (element) {
        $(element).valid();
    },
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 255
        },
        password: {
            required: true,
            maxlength: 20,
        }
    },
    messages: {
        email: {
            email : 'メールアドレスを正しく入力してください。',
        },
    },
    submitHandler: function (form) {
        form.submit();
        $('input[type="submit"]').attr('disabled', 'disabled');
    }
});