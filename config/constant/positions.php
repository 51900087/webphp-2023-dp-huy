<?php
use App\Models\User;
    return [
        'id' => [
            0 => '0',
            1 => '1',
            2 => '2',
            3 => '3',
        ],
        'name' => [
            0 => 'General Director',
            1 => 'Group Leader',
            2 => 'Leader',
            3 => 'Member',
        ],
    ];