<?php

namespace App\Exports;

use App\Models\Division;
use App\Models\User;
use Config;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
class UserExport implements FromCollection, WithHeadings, WithCustomCsvSettings
{
    protected $data;
    public function getCsvSettings(): array
    {
        return [
            'enclosure' => ''
        ];
    }

    public function __construct($data)
    {
        foreach ($data as &$value) {
            $value = stripcslashes($value);
        }
        $this->data = $data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $userModel = new User();
        $type ='csv';
        $users = $userModel->search($this->data, $type);
        if($users){
            $users = $users->map(function ($user) {
                $position_name = config('constant.positions.name');
                $divisionModel = new Division();
                $get_division_name = $divisionModel->getDivisionById($user->division_id);
                $division_name = $get_division_name->name ?? '';
                $user->position_id = (string)$user->position_id;
                $user->entered_date = date('Y/m/d', strtotime($user->entered_date));
                $user->created_date = date('Y/m/d', strtotime($user->created_date));
                $user->updated_date = date('Y/m/d', strtotime($user->updated_date));
                $userResult =  [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'division_id' => $user->division_id,
                    'division_name' => $division_name,
                    'entered_date' => $user->entered_date,
                    'position_id' => $position_name[$user->position_id] ?? '',
                    'created_date' => $user->created_date,
                    'updated_date' => $user->updated_date,
                ];
                return $userResult;
            });
            return $users;
        }
    }



    public function headings(): array
    {
        $headings = [
            "ID",
            "User Name",
            "Email",
            "Division ID",
            "Division Name",
            "Entered Date",
            "Position",
            "Created Date",
            "Updated Date",
        ];
        return $headings;
    }
}
