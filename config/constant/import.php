<?php

    return [
        'division' => [
            'id' => [
                'name' => 'ID',
            ],
            'division_name' => [
                'max_length' => 255,
                'name' => 'Division Name',
            ],
            'division_leader_id' => [
                'name' => 'Division Leader',
            ],
            'floor_number' => [
                'name' => 'Floor Number',
            ],
            'division_note' => [
                'name' => 'Division Note',
            ],
            'delete' => [
                'name' => 'Delete',
                'in' => 'Y',
            ],
        ],
    ];