$('#moduleUserUpdate').validate({
    ignore: [],
    onfocusout: function (element) {
        $(element).valid();
    },
    onkeyup: function (element) {
        $(element).valid();
    },
    onkeyup: function (element) {
        if ($(element).attr('name') == 'password') {
            if($(element).val() == '' && $('#confirmPassword').val() == '') {
                $('#password').rules('remove');
                $('#confirmPassword').rules('remove');
            } else {
                $('#password').rules('add', {
                    required: true,
                    checkByteWordUpdate : true,
                    maxlength: 20,
                    checkLengthUpdate: true,
                    regExpression: true,
                });
                $('#confirmPassword').rules('add', {
                    required: true,
                    equalTo: '#password',
                    maxlength: 20,
                });
            }
        } 
        if($(element).attr('name') == 'confirmPassword'){
            if($(element).val() == '' && $('#password').val() == '') {
                $('#password').rules('remove');
                $('#confirmPassword').rules('remove');
            } else {
                $('#password').rules('add', {
                    required: true,
                    checkByteWordUpdate : true,
                    maxlength: 20,
                    checkLengthUpdate: true,
                    regExpression: true,
                });
                $('#confirmPassword').rules('add', {
                    required: true,
                    equalTo: '#password',
                    // checkByteWordUpdate : true,
                    // checkLengthUpdate: true,
                    maxlength: 20,
                });
            }
        }
        $(element).valid();
    },
    onfocusout: function (element) {
        if ($(element).attr('name') == 'password') {
            if ($(element).val() == '' && $('#confirmPassword').val() == '') {
                $('#password').rules('remove');
                $('#confirmPassword').rules('remove');
            } else {
                $('#password').rules('add', {
                    required: true,
                    checkByteWordUpdate : true,
                    maxlength: 20,
                    checkLengthUpdate: true,
                    regExpression: true,
                });
                $('#confirmPassword').rules('add', {
                    required: true,
                    equalTo: '#password',
                    maxlength: 20,
                });
                $(element).valid();
            }
        } else{
            $(element).valid();
        }
    }, 
    onchange: function (element) {
        if($(element).attr('name') == 'password' || $(element).attr('name') == 'confirmPassword'){
            if($(element).val() == ''){
                $('#password').rules('remove');
                $('#confirmPassword').rules('remove');
            } else {
                $('#password').rules('add', {
                    required: true,
                    checkByteWordUpdate : true,
                    maxlength: 20,
                    checkLengthUpdate: true,
                    regExpression: true,
                });
                $('#confirmPassword').rules('add', {
                    required: true,
                    equalTo: '#password',
                    checkByteWordUpdate : true,
                    maxlength: 20,
                    checkLengthUpdate: true,
                    regExpression: true,
                });
                $(element).valid();
            }
        }
    },
    rules: {
        username: {
            required: true,
            maxlength: 50,
            // checkByteWordUpdate: true,
        },
        email: {
            required: true,
            checkByteEmailUpdate: true,
            email: true,
            maxlength: 255
        },
        entered_date: {
            required: true,
            dateCheck: true,
        },
        'position[]': {
            required: true,
        },
        'division[]': {
            required: true,
        },
    },
    messages: {
        email: {
            email: 'メールアドレスを正しく入力してください。',
        },
    },
    submitHandler: function (form) {
        form.submit();
        $('input[type="submit"]').attr('disabled', 'disabled');
    }
});