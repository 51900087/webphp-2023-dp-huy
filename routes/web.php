<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DivisionController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('login')->group(function() {
    Route::get('/', [AuthController::class, 'login'])->name('login.get');
    Route::post('/', [AuthController::class, 'checkLogin'])->name('login.post');
});

// Route::group(['middleware' => 'prevent-back-history'], function () {
Route::get('logout', [AuthController::class, 'logout'])->name('logout')->middleware('prevent-back-history');
// });

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', [UserController::class, 'index'])->name('home');
    Route::post('exportCSV', [UserController::class, 'exportCSV'])->name('exportCSV');
    Route::get('search', [UserController::class, 'search'])->name('search');
    Route::prefix('module')->group(function() {
        Route::get('create', [UserController::class, 'create'])->name('module.create');
        Route::post('store', [UserController::class, 'store'])->name('module.store');
        Route::get('edit/{id}', [UserController::class, 'edit'])->name('module.edit');
        Route::post('/update/{id}', [UserController::class, 'update'])->name('module.update');
        Route::get('delete/{id}', [UserController::class, 'destroy'])->name('module.destroy');
    });
    Route::prefix('division')->group(function() {
        Route::get('/', [DivisionController::class, 'index'])->name('division.index');
        Route::post('import', [DivisionController::class, 'import'])->name('division.import');
    });
});