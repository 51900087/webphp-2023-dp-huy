$("#password").on('keyup', function () {
    $("#confirmPassword").valid();
});
$('#moduleUserRegister').validate({
    onfocusout: function (element) {
        $(element).valid();
    },
    onkeyup: function (element) {
        $(element).valid();
    },
    ignore: [],
    rules: {
        username: {
            required: true,
            maxlength: 50,
            // checkByteWordRegister: true,
        },
        email: {
            required: true,
            checkByteEmailRegister: true,
            email: true,
            maxlength: 255,
        },
        entered_date: {
            required: true,
            dateCheck: true,
        },
        'division[]': {
            required: true,
            
        },
        'position[]': {
            required: true,
        },
        password: {
            required: true,
            maxlength: 20,
            checkLengthRegister: true,
            regExpression: true,
            checkByteWordRegister: true,
        },
        confirmPassword: {
            required: true,
            maxlength: 20,
            equalTo: '#password',
            regExpression: true,
            checkByteWordRegister: true,
        },
    },
    messages: {
        email: {
            email: 'メールアドレスを正しく入力してください。',
        },
    },
    //when submit form, avoid multiple submit, disable button until ajax finish
    submitHandler: function (form) {
        var $form = $(form);
        var $button = $form.find('button[type="submit"]');
        $button.attr('disabled', 'disabled');
        $button.addClass('disabled');
        $button.find('i').removeClass('hide');
        form.submit();
    }
});
