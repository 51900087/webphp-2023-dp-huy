<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>@yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    {{-- <link href="{{('assets/img/favicon.png')}}" rel="icon"> --}}
    {{-- <link href="{{('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> --}}
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <style type="text/css">
        .input-group-text {
            width: 200px;
        }
        td {
            max-width: 10px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <header id="header" class="header fixed-top d-flex align-items-center">
        <div class="d-flex align-items-center justify-content-between">
            <i class="bi bi-list toggle-sidebar-btn"></i>
        </div>
        @php
        $user_login = Auth::user();
        @endphp
        <style type="text/css">
            .center {
                position: absolute;
                left: 50%;
                transform: translate(-50%, -50%);
                top: 60%;
            }

            td {
                max-width: 150px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
            .nav-link.active {
                color: #fff;
                background-color: #0d6efd;
                }

        </style>
        <nav class="header-nav ms-auto">
            <ul class="d-flex align-items-center">
                {{-- center of screen --}}
                <li class="center">
                    <h3>@yield('title')</h3>
                </li>
                <li class="nav-item dropdown pe-3">
                    <span class=""><label>{{Illuminate\Support\Str::limit($user_login->name ?? '', 20)}}</label></span>
                    <a class="" href="{{route('logout')}}">Logout</a>
                </li>
            </ul>
        </nav>
    </header>
    <aside id="sidebar" class="sidebar">
        <ul class="sidebar-nav" id="sidebar-nav">
            <li class="nav-item" >
                @if(Request::is('/') || Request::is('module/*') || Request::is('search'))
                    <a class="nav-link active" href="{{route('home')}}">
                @else 
                    <a class="nav-link" href="{{route('home')}}">
                @endif
                <span>User List</span>
                </a>
            </li>
            <li class="nav-item">
                @php
                    $user_login = Auth::user();
                @endphp
                @if (isset($user_login) && $user_login->position_id == $positionData['id'][0])
                    <a class="nav-link {{Request::is('division') ? 'active' : '' }}" href="{{route('division.index')}}">
                        <span>Division List</span>
                    </a>
                @endif 
            </li>
        </ul>
    </aside>
    @show
    <main id="main" class="main">
        @yield('admin_content')
        @include('sweetalert::alert')
    </main>
    <script src="{{asset('assets/js/template/main.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('.nav-link').click(function() {
                $('.nav-link').removeClass('active');
                $(this).addClass('active');
            });
        });
    </script>
</body>
</html>