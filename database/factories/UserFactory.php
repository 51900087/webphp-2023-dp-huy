<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'password' => Hash::make('12345678'), 
            'name' => $this->faker->name,
            'division_id' => $this->faker->numberBetween(1,9999),
            'entered_date' => $this->faker->date(),
            'position_id' => $this->faker->numberBetween(0,3),
            'created_date' => $this->faker->dateTime(),
            'updated_date' => $this->faker->dateTime(),
            'deleted_date' => null,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
