@extends('layout.admin')
@section('title', 'User List')
@section('sliderbar')
@section('admin_content')
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('assets/js/common/errors.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<meta name="_token" content="{{ csrf_token() }}">
<style>
	/* display id=errors center of screen */
	#errors {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		/* increase size of error box */
		width: 700px;
		height: 100px;
	}
</style>
@if(Session::has('errors'))
<div id="errors" class="card-body">
		<center id="center"  class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong><h1>{{ $errors }}</h1></strong>
		</center>
</div>
@elseif(Session::has('error'))
<div id="errors" class="card-body">
	<center id="center"  class="alert alert-danger alert-dismissible fade show" role="alert">
		<strong><h1>{{ Session::get('error') }}</h1></strong>
		@php 
			Session::forget('error');
		@endphp
	</center>
</div>
@else
<div class="card">
	<div class="card-body">
		<h5 class="card-title"></h5>
		<form class="row g-3" id="listUser" action="{{route('search')}}">
			<div class="col-md-6">
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">User Name</span>
					<input type="text" class="form-control" id="searchName"  value="{{$data['username'] ?? ''}}" name="username" data-label="User Name"  aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<div class="input-group mb-3">
					<div id="usernameError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Entered Date From</span>
					<input type="text" class="form-control" id="enteredDateFrom"  value="{{$data['enteredDateFrom'] ?? ''}}" name="enteredDateFrom" data-label="Entered Date From"  aria-label="enterdDateFrom">
					</div>
				<div class="input-group mb-3">
					<div  id="enteredDateFromError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Entered Date To</span>
					<input type="text" class="form-control" value="{{$data['enteredDateTo'] ?? ''}}" id="enteredDateTo" name="enteredDateTo" data-label="Entered Date To"  aria-label="enteredDateTo">
				</div>
				<div class="input-group mb-3">
					<div id="enteredDateToError" class="error"></div>
				</div>
			</div>
			<div class="text-center">
				<button type="button" id="btnClear" class="btn btn-secondary">Clear</button>
				<button type="submit" id="btnSearch"  class="btn btn-primary">Search</button>
			</div>
		</form>
	</div>
		<div class="card-body">
			@if(!empty($users))
			<style type="text/css">
				#center {
					margin-top: 20px;
					margin-bottom: 20px;
					display: flex;
					justify-content: center;
					align-items: center;
				}
			</style>
			<center id="center">
				{{$users->appends($data)->links('vendor.pagination.bootstrap-4')}}
			</center>
			@if($users->count() <= 0)
			<center id="center"  class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong> No Record</strong>
			</center>
			@else
			<table id="table" class="table table-bordered table-striped table-hover">
				<thead>
					<tr>
					<th scope="col"><label>User Name<label></th>
					<th scope="col"><label>Email<label></th>
					<th scope="col"><label>Division Name<label></th>
					<th scope="col"><label>Entered Date<label></th>
					<th scope="col"><label>Position<label></th>
					</tr>
				</thead>
				<tbody>
					@php 
						$numberLimit = 30;
					@endphp
					@foreach ($users as $user)
					<tr>
						<td ><a href="{{ route('module.edit', $user->id) }}">{{ Illuminate\Support\Str::limit($user->name,  $numberLimit)}}</a></td>
						<td>{{ Illuminate\Support\Str::limit($user->email,  $numberLimit) }}</td>
						@php 	
							$divisionModel = new App\Models\Division();
							$divisionData = $divisionModel->getDivisionById($user->division_id);	
						@endphp
						<td>{{ Illuminate\Support\Str::limit($divisionData->name ?? '' , $numberLimit)}}</td>
						@php
							$user->entered_date = date('Y/m/d', strtotime($user->entered_date));
						@endphp
						<td><label>{{ $user->entered_date }}</label></td>
						@php
						if($user->position_id == $positionData['id'][0]) {
						$user->position = $positionData['name'][0];
						} else if($user->position_id == $positionData['id'][1]) {
						$user->position = $positionData['name'][1];
						} else if($user->position_id == $positionData['id'][2]) {
						$user->position = $positionData['name'][2];
						} else if($user->position_id == $positionData['id'][3]){
						$user->position = $positionData['name'][3];
						}  else {
						$user->position = '';
						}
						@endphp
						<td>{{ Illuminate\Support\Str::limit($user->position ,  $numberLimit)}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@endif
			@endif
			<div class="row g-3">
				@php
					$user_login = Auth::user();
					if (isset($user_login) && $user_login->position_id == $positionData['id'][0]) {
				@endphp
					<div class="col-md-2">
						<div class="input-group mb-3">
							<button type="button" onclick="location.href='{{ route('module.create') }}'" class="btn btn-success">New</button>
						</div>
					</div>
					@if(!empty($users))
					@php
						if($users->count() <= 0) {
					@endphp
						<div class="col-md-2">
						</div>
					@php
						} else {
					@endphp
						<div class="col-md-2">
							<button type="button" id="btnExportCSV" class="btn btn-danger" >Output CSV</button>  
						</div>
					@php 
						}
					@endphp
											
					@endif
						<div class="col-md-8">
						</div>
				@php
					}
				@endphp
			</div>
	</div>
</div>
@endif
<script type="text/javascript" src="{{asset('assets/js/list/search.js')}}"></script>
<script type="text/javascript">
	$('#btnClear').click(function(){
		// location.reload();
		$('input[name=username]').val('');
		$('input[name=enteredDateFrom]').val('');
		$('input[name=enteredDateTo]').val('');
		window.location.reload();
	})
</script>
<script type="text/javascript">
	$('#btnExportCSV').click(function(){
        var username = $('input[name=username]').val();
        var enteredDateFrom = $('input[name=enteredDateFrom]').val();
        var enteredDateTo = $('input[name=enteredDateTo]').val();
        var data = {
            'username': username,
            'enteredDateFrom': enteredDateFrom,
            'enteredDateTo': enteredDateTo
        }
		//click many times but only one request
		$(this).prop('disabled', true);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url: "{{route('exportCSV')}}",
            data: data,
            success:function(data){
                var link = document.createElement('a');
                var binaryData = [];
                binaryData.push(data);
                link.href = window.URL.createObjectURL(new Blob(binaryData, {type: "application/zip"}));
				var timestamp = new Date().getTime();
				var date = new Date(timestamp);
				var year = date.getFullYear();
				var month = date.getMonth() + 1;
				if(month < 10) {
					month = '0' + month;
				}
				var day = date.getDate();
				if(day < 10) {
					day = '0' + day;
				}
				var hour = date.getHours();
				if(hour < 10) {
					hour = '0' + hour;
				}
				var minute = date.getMinutes();
				if(minute < 10) {
					minute = '0' + minute;
				}
				var second = date.getSeconds();
				if(second < 10) {
					second = '0' + second;
				}
				var yyyymmddhhmmss = year + '' + month + '' + day + '' + hour + '' + minute + '' + second;
				link.download = `list_user_${yyyymmddhhmmss}.csv`;
                link.click();
				swal("成功", "CSVファイルを出力しました", "success");
				$('#btnExportCSV').prop('disabled', false);
            },
            error: function (data) {
                swal("Oops!", "Something went wrong", "error");
				$('#btnExportCSV').prop('disabled', false);
            }
        });
	})
</script>
@endsection