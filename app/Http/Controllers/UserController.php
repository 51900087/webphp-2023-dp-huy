<?php

namespace App\Http\Controllers;

use App\Models\Division;
use App\Models\User;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    
    // public function checkPosition(Request $request)
    // {
    //     $positionData = config('constant.positions');
    //     $position_id_logged = Auth::user()->position_id;
    //     if($position_id_logged != $positionData['id'][0]){
    //         Auth::logout();
    //         //forget session
    //         session()->forget('user');
    //         session()->flush();
    //         $request->session()->invalidate();
    //         return redirect()->route('login.get');
    //     }
    // }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positionData = config('constant.positions');
        return view('user.list')->with('positionData', $positionData);
    }

    public function search(Request $request)
    {
        $data = $request->only('username','enteredDateFrom','enteredDateTo');
        $userModel = new User();
        //check exist
        if(!isset($data['username'])){
            $data['username'] = null;
        }
        if(!isset($data['enteredDateFrom'])){
            $data['enteredDateFrom'] = null;
        }
        if(!isset($data['enteredDateTo'])){
            $data['enteredDateTo'] = null;
        }
        $type  ='';
        $users = $userModel->search($data,$type);
        $positionData = config('constant.positions');
        return view('user.list')->with('users', $users)->with('data', $data)->with('positionData', $positionData);
    }


    public function getAllDivisions()
    {
        $divisionModel = new Division();
        $divisions = $divisionModel->getAllDivisions();
        return $divisions;
    }
    public function create()
    {
        $positionData = config('constant.positions');
        $position_id_logged = Auth::user()->position_id;
        if($position_id_logged != $positionData['id'][0]){
            Auth::logout();
            session()->forget('user');
            session()->flush();
            session()->invalidate();
            return redirect()->route('login.get');
        }
        $divisionModel = new Division();
        $divisions = $divisionModel->getAllDivisions();
        return view('user.module')->with('positionData', $positionData)->with('divisions', $divisions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['position'] = $data['position'][0];
        if(is_numeric($data['division_hidden'])){
            $data['division'] = $data['division_hidden'];
        } else {
            $data['division'] = null;
        }
        $data['position'] = (int)$data['position'];
        $data['division'] = (int)$data['division'];
        $userModel = new User();
        $result = $userModel->add($data);
        if ($result == true) {
            $messageSCL001 = config('constant.messages.success.SCL001');
            Session()->flash('success', $messageSCL001);
            return redirect()->route('module.create');
        } else {
            $messageECL_EMAIL_EXIST = config('constant.messages.error.ECL_EMAIL_EXIST');
            Session()->flash('error', $messageECL_EMAIL_EXIST);
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $positionData = config('constant.positions');
        $position_id_logged = Auth::user()->position_id;
        if($position_id_logged != $positionData['id'][0] && Auth::user()->id != $id){
                Auth::logout();
                session()->forget('user');
                session()->flush();
                session()->invalidate();
                return redirect()->route('login.get');
        }
        $userModel = new User();
        $user = $userModel->getUserByIDWithoutDelete($id);
        if($user){
            $user->entered_date = date('Y/m/d', strtotime($user->entered_date));
            $divisionModel = new Division();
            $divisions = $divisionModel->getAllDivisions();
            return view('user.module')->with('user', $user)->with('positionData', $positionData)->with('divisions', $divisions);
        } else {
            $temp = Session()->get('error');
            if(Session()->has('error')){
                Session()->flash('error', $temp);
                return redirect()->route('home');
            } else {
                $messageECL_not_found= config('constant.messages.error.ECL_not_found');
                Session()->flash('errors', $messageECL_not_found);
                return redirect()->route('home');
            //}
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'username' => 'required',
            'position' => 'required',
            'division' => 'required',
        ]);
        //RELOAD db
        DB::reconnect();
        //get user and check if user is deleted, if deleted, return error
        $userModel = new User();
        $getUser = $userModel->getUserById($id);
        if($getUser->deleted_date != null){
            $messageECL_CRUD_Failed = config('constant.messages.error.ECL_CRUD_Failed');
            Session()->flash('error', $messageECL_CRUD_Failed);
            return redirect()->back();
        } else {
                    //check email is already exist in db
            $checkEmail = $userModel->checkEmailInSystemUpdate($id, $request->email);
            if($checkEmail == false){
                $messageECL_EMAIL_EXIST = config('constant.messages.error.ECL_EMAIL_EXIST');
                Session()->flash('error', $messageECL_EMAIL_EXIST);
                return redirect()->back();
            }
            $positionData = config('constant.positions');
            $position_id_logged = Auth::user()->position_id;
            if($position_id_logged != $positionData['id'][0] && Auth::user()->id == $id){
                $data = $request->only('password');
            } else {
                $data = $request->all();
                if(is_numeric($data['division_hidden'])){
                    $data['division'] = $data['division_hidden'];
                } else {
                    $data['division'] = null;
                }
            }
            $userModel->updateUser($data, $id);
            if ($userModel) {
                //update Session if user update himself
                if(Auth::user()->id == $id){
                    $user = $userModel->getUserById($id);
                    session()->put('user', $user);
                }
                $position_id_logged = Auth::user()->position_id;
                if($position_id_logged != $positionData['id'][0] && Auth::user()->id != $id){
                        Auth::logout();
                        session()->forget('user');
                        session()->flush();
                        session()->invalidate();
                        return redirect()->route('login.get');
                }
                $messageSCL001 = config('constant.messages.success.SCL001');
                Session()->flash('success', $messageSCL001);
                return redirect()->route('module.edit', $id);
            } else {
                $messageECL001 = config('constant.messages.error.ECL001');
                Session()->flash('error', $messageECL001);
                return redirect()->back();
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::reconnect();
        $userModel = new User();
        $user = $userModel->getUserByIDWithoutDelete($id);
        if($user){
            $result = $userModel->remove($id);
            if($result && Auth::user()->id == $id){
                Auth::logout();
                session()->forget('user');
                session()->flush();
                session()->invalidate();
                return redirect()->route('login.get');
            }
            return redirect()->route('home');
        } else {
            $messageECL_CRUD_Failed = config('constant.messages.error.ECL_CRUD_Failed');
            // Session()->flash('error', $messageECL_CRUD_Failed);
            // //return error
            return response()->json([
                'status' => 'error',
                'message' => $messageECL_CRUD_Failed,
            ], 500);
        }
    }

    public function exportCSV(Request $request)
    {
        $data = $data = $request->only('username','enteredDateFrom','enteredDateTo');
        return Excel::download(new UserExport($data), 'users.csv');
    }
}
