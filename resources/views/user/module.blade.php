@extends('layout.admin')
@if(isset($user))
	@section('title', 'User Edit Delete')
@else
	@section('title', 'User Add')
@endif
@section('sliderbar')
@section('admin_content')
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('assets/js/common/errors.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<div class="card">
	@php
		$user_login = Auth::user();
	@endphp
	<div class="card-body">
		<h5 class="card-title"></h5>
		@if (session('success'))
			<script>
				swal({
					title: "成功!",
					text: "{{session('success')}}",
					icon: "success",
					button: "OK",
				})
				. then(function() {
					window.location.href = "{{route('home')}}";
				});
			</script>
		@endif
		@if (session('error'))
			<script>
				swal({
					title: "失敗!",
					text: "{{session('error')}}",
					icon: "error",
					button: "OK",
				})
				. then(function() {
					window.location.href = "{{route('home')}}";
				});
			</script>
		@endif
		@if (isset($user_login) && $user_login->position_id == $positionData['id'][0] && !isset($user))
		<form id="moduleUserRegister" class="row g-3" action="{{route('module.store')}}" method="POST">	
		@endif
		@if (isset($user_login) && isset($user))
		<form id="moduleUserUpdate" class="row g-3" method="POST" action="{{route('module.update', $user->id)}}">		
		@endif
		@csrf
		<div class="col-md-6">
			<input type="hidden" name="id" value="{{$user->id ?? ''}}">
			<div class="input-group mb-3">
			<span class="input-group-text" id="basic-addon1">ID</span>
			@if(isset($user))
				<input type="text" disabled class="form-control" value="{{$user->id ?? 'ID'}}"  aria-label="Username" aria-describedby="basic-addon1">
			@endif
			</div>
		</div>
		@if (isset($user_login) && $user_login->position_id == $positionData['id'][0])
			@if(isset($user))
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">User Name</span>
					<input type="text" class="form-control" id="username" name="username" data-label="User Name" value="{{$user->name ?? ''}}"  aria-label="Username" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="usernameError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Email</span>
					<input type="text" class="form-control" name="email" data-label="Email" value="{{$user->email ?? ''}}" aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="emailError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
						<label class="input-group-text" for="inputGroupSelect01">Division</label>
						<input type="hidden" classlabel="form-control" id="division_hidden" name="division_hidden" value="{{$user->division_id ?? ''}}"  data-label="Division" aria-describedby="basic-addon1">
						<select name="division[]" data-label="Division" class="form-select" id="division">
							<option  value="" selected>Choose...</option>
							@foreach ($divisions as $key => $division)
								@php
									if(isset($user)){
										$divisionUser = $user->division->id ?? '';
									} else {
										$divisionUser = '';
									}
								@endphp
								@if(isset($user) && $divisionUser == $key)
									<option value="{{$key}}" data-label="{{$division}}" selected>{{$division}}</option>
								@else
								<option data-label="{{$division}}" value="{{$key}}">{{$division}}</option>
								@endif
							@endforeach
						</select> 
					</div>
					<div class="input-group mb-3">
						<div  id="divisionError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Entered Date</span>
					<input type="text" class="form-control" name="entered_date" data-label="Entered Date" value="{{$user->entered_date ?? ''}}"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="enteredDateError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
						<div class="input-group mb-3">
							<label class="input-group-text" for="inputGroupSelect02">Position</label>
							@if (isset($user_login) && $user_login->position_id == $positionData['id'][0] && !isset($user))
							<input  type="hidden"  class="form-control" id="position" name="position"  data-label="Position" aria-describedby="basic-addon1">
							@endif
							<select class="form-select" name="position[]"  data-label="Position" id="inputGroupSelect02">
								<option  value="" selected>Choose...</option>
								@foreach ($positionData['name'] as $key => $position)
									@if(isset($user) && $user->position_id == $key)
										<option value="{{$key}}" data-label="{{$position}}" selected>{{$position}}</option>
									@else
									<option value="{{$key}}">{{$position}}</option>
									@endif
								@endforeach
							</select> 
						</div>
			
					</div>
					<div class="input-group mb-3">
						<div  id="positionError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Password</span>
					<input type="password" class="form-control"  id="password" name="password" data-label="Password" aria-label="enterdDateFrom" aria-describedby="basic-addon1">
				</div>
					<div class="input-group mb-3">
						<div  id="passwordError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Password Confirmation</span>
					<input type="password" class="form-control"  name="confirmPassword" id="confirmPassword" data-label="Password Confirmation"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="confirmPasswordError" class="error"></div>
					</div>
				</div>
			@else
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">User Name</span>
					<input type="text" class="form-control" id="username" name="username" data-label="User Name"  aria-label="Username" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="usernameError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Email</span>
					<input type="text" class="form-control" name="email" data-label="Email" aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="emailError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
						<label class="input-group-text" for="inputGroupSelect01">Division</label>
						<input type="hidden" class="form-control" id="division_hidden" name="division_hidden"  aria-describedby="basic-addon1">
						<select name="division[]" data-label="Division" class="form-select" id="division">
							<option value="" selected>Choose...</option>
							@foreach ($divisions as $key => $division)
								<option data-label="{{$division}}" value="{{$key}}">{{$division}}</option>
							@endforeach
						</select> 
					</div>
					<div class="input-group mb-3">
						<div  id="divisionError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Entered Date</span>
					<input type="text" class="form-control" name="entered_date" data-label="Entered Date"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="enteredDateError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
						<div class="input-group mb-3">
							<label class="input-group-text" for="inputGroupSelect02">Position</label>
							@if (isset($user_login) && $user_login->position_id == $positionData['id'][0] && !isset($user))
							<input  type="hidden"  class="form-control" id="position" name="position"  aria-describedby="basic-addon1">
							@endif
							<select  data-label="Position" class="form-select" name="position[]" id="inputGroupSelect02">
								<option  value="" selected>Choose...</option>
								@foreach ($positionData['name'] as $key => $position)
									<option value="{{$key}}">{{$position}}</option>
								@endforeach
							</select> 
						</div>
			
					</div>
					<div class="input-group mb-3">
						<div  id="positionError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Password</span>
					<input type="password" class="form-control"  id="password" name="password" data-label="Password" name="enterdDateFrom"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="passwordError" class="error"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group mb-3">
					<span class="input-group-text" id="basic-addon1">Password Confirmation</span>
					<input type="password" class="form-control"   name="confirmPassword" id="confirmPassword" data-label="Password Confirmation"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
					</div>
					<div class="input-group mb-3">
						<div  id="confirmPasswordError" class="error"></div>
					</div>
				</div>
			@endif
		@endif
		@if(isset($user_login) && $user_login->position_id != $positionData['id'][0] &&  isset($user))
			<div class="col-md-6">
				<div class="input-group mb-3">
				<span class="input-group-text" id="basic-addon1">User Name</span>
				<input type="text" class="form-control" id="username" name="username" data-label="User Name" value="{{$user->name ?? ''}}"  aria-label="Username" aria-describedby="basic-addon1" disabled>
				</div>
				<div class="input-group mb-3">
					<div  id="usernameError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
				<span class="input-group-text" id="basic-addon1">Email</span>
				<input type="text" class="form-control" name="email" data-label="Email" value="{{$user->email ?? ''}}" aria-label="enterdDateFrom" aria-describedby="basic-addon1" disabled>
				</div>
				<div class="input-group mb-3">
					<div  id="emailError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
					<label class="input-group-text" for="inputGroupSelect01">Division</label>
					<input type="hidden" class="form-control" id="division_hidden" name="division_hidden" value="{{$user->division_id ?? ''}}"  data-label="Division" aria-describedby="basic-addon1">
					<select disabled name="division[]"  class="form-select" id="division">
						<option  value="" selected>Choose...</option>
						@foreach ($divisions as $key => $division)
						@php
							if(isset($user)){
								$divisionUser = $user->division->id ?? '';
							} else {
								$divisionUser = '';
							}
						@endphp
						@if(isset($user) && $divisionUser == $key)
							<option value="{{$key}}" data-label="{{$division}}" selected>{{$division}}</option>
						@else
						<option data-label="{{$division}}" value="{{$key}}">{{$division}}</option>
						@endif
					@endforeach
					</select> 
				</div>
				<div class="input-group mb-3">
					<div  id="divisionError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
				<span class="input-group-text" id="basic-addon1">Entered Date</span>
				<input disabled type="text" class="form-control" name="entered_date" data-label="Entered Date" value="{{$user->entered_date ?? ''}}"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
				</div>
				<div class="input-group mb-3">
					<div  id="enteredDateError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
					<div class="input-group mb-3">
						<label class="input-group-text" for="inputGroupSelect02">Position</label>
						@if (isset($user_login) && $user_login->position_id == $positionData['id'][0] && !isset($user))
						<input  type="hidden"  class="form-control" id="position" name="position"  data-label="Position" aria-describedby="basic-addon1">
						@endif
						<select disabled class="form-select" name="position[]" id="inputGroupSelect02">
							<option  value="" selected>Choose...</option>
							@foreach ($positionData['name'] as $key => $position)
								@if(isset($user) && $user->position_id == $key)
									<option value="{{$key}}" data-label="{{$position}}" selected>{{$position}}</option>
								@else
								<option value="{{$key}}">{{$position}}</option>
								@endif
							@endforeach
						</select> 
					</div>
				</div>
				<div class="input-group mb-3">
					<div  id="positionError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
				<span class="input-group-text" id="basic-addon1">Password</span>
				<input  type="password" class="form-control"  id="password" name="password" data-label="Password"  name="enterdDateFrom"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
				</div>
				<div class="input-group mb-3">
					<div  id="passwordError" class="error"></div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="input-group mb-3">
				<span class="input-group-text" id="basic-addon1">Password Confirmation</span>
				<input  type="password" class="form-control"  name="confirmPassword" id="confirmPassword" data-label="Password Confirmation"  aria-label="enterdDateFrom" aria-describedby="basic-addon1">
				</div>
				<div class="input-group mb-3">
					<div  id="confirmPasswordError" class="error"></div>
				</div>
			</div>
		@endif
		<div class="col-md-3">
			@if (isset($user))
				<div></div>
			@else
				<button	button type="submit" id="btnRegister" class="btn btn-primary">Register</button>
			@endif
		</div>
		<div  class="col-md-3">
			@if (!isset($user))
				<div></div>
			@else
				<button type="submit" id="btnUpdate" class="btn btn-success">Update</button>
				</div>
				<div  class="col-md-3">
				<button type="button" id="btnDelete" class="btn btn-danger">Delete</button>
			@endif
		</div>
		<div  class="col-md-3">
			<button type="button" onclick="window.location.href='{{route('home')}}'" id="btnCancel" class="btn btn-warning">Cancel</button>
			
		</div>
		</div>
	</form>
	</div>
</div>
@if (isset($user_login) && $user_login->position_id == $positionData['id'][0] && !isset($user))
<script type="text/javascript" src="{{asset('assets/js/module/register.js')}}"></script>
@endif
@if (isset($user_login) && isset($user))
<script type="text/javascript" src="{{asset('assets/js/module/update.js')}}"></script>
{{-- <script type="text/javascript">
	$('#password').change(function(){
		$('input[name="password_hidden"]').val($('#password').val());
		$('input[name="confirmPassword_hidden"]').val('');
		
	});
	$('#confirmPassword').change(function(){
		$('input[name="confirmPassword_hidden"]').val($('#confirmPassword').val());
		alert($('#password_hidden').val() + 'and' + $('#confirmPassword_hidden').val());
	});
</script> --}}
<script type="text/javascript">
	$('#btnDelete').click(function(){
		var id = $('input[name="id"]').val();
		if(id == {{$user_login->id}}){
			swal("すでに証明書番号は登録されています。", {
				icon: "error",
			});
		}else{
			var url = "{{route('module.destroy', ['id' => 'id'])}}";
			url = url.replace('id', id);
			swal({
				title: "このユーザーを削除しますか？",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: url,
						type: 'GET',
						success: function(data){
							swal({
								title: "削除しました",
								text: "ユーザーを削除しました",
								icon: "success",
							}) .then((willDelete) => {
								if (willDelete) {
									window.location.href = "{{route('home')}}";
								} else {
									window.location.href = "{{route('home')}}";
								}
							});
						},
						error: function(data){
							swal("登録・更新・削除処理に失敗しました。", {
								icon: "error",
							}).then((value) => {
								window.location.href = "{{route('home')}}";
							});
						}
					});
				}
			});
		}
	});
</script>
@endif
<script type="text/javascript">
	$('#division').change(function(){
		var value = $(this).val();
		$('#division_hidden').val(value);
		// $('#division_value').val(label);
	});
	$('#inputGroupSelect02').change(function(){
		var value = $(this).val();
		$('#position').val(value);
	});
</script>
@endsection