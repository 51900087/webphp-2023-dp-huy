<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;

    protected $table = 'division';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'note',
        'division_leader_id',
        'division_floor_num',
        'created_date',
        'updated_date',
        'deleted_date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'division_leader_id', 'id');
    }
    
    public function getDivisionById($id)
    {
        //get divsion_name by id
        return $this->where('id', $id)->whereNull('deleted_date')->first();
    }

    
    public function getAllDivisions()
    {
        $result = $this->whereNull('deleted_date')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        // $result = array_unique($result);
        return $result;

    }

    //get user by division leader id


}
