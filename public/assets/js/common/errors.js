var error_maxlength = jQuery.validator.format("{0}は「{1}」文字以下で入力してください。（現在{2}文字）");
var maxLengtEmail ='255';   
var maxLengthPassword ='20';
var minlengthPassword ='8'; 
var maxLengthNameSearch ='100';
var maxLengthName ='50';
var require = jQuery.validator.format("{0}は必須項目です。");
var error_length_Register = jQuery.validator.format("パスワードは半角英数字記号で8～20文字で入力してください。");
var error_length_Update = jQuery.validator.format("パスワードは半角英数字記号で8～20文字で入力してください。");
var error_date = jQuery.validator.format("解約予定日は契約終了日前を指定してください。");
var error_date_format = jQuery.validator.format("{0}は日付を正しく入力してください。");
var error_email1ByteUpdate = jQuery.validator.format("{0}は半角英数で入力してください。");
var error_email1ByteRegister = jQuery.validator.format("{0}は半角英数で入力してください。");
var error_equalTo = jQuery.validator.format("確認用のパスワードが間違っています。");
var error_regExpression = jQuery.validator.format("{0}は半角英数字で入力してください。");
var error_byteWordRegister = jQuery.validator.format("{0}は全角カナで入力してください。");
var error_byteWordUpdate = jQuery.validator.format("{0}は半角英数で入力してください。");
$.validator.messages.maxlength = function (param,input) {
    data = $(input).data('label');
    inputLength = $(input).val().length;
    if ($(input).attr('name') == 'email') {
        return error_maxlength(data, maxLengtEmail, inputLength);
    }
    if ($(input).attr('name') == 'password' || $(input).attr('name') == 'confirmPassword') {
        return error_maxlength(data, maxLengthPassword, inputLength);
    }
    if ($(input).attr('name') == 'username') {
        if ($(input).attr('id') == 'searchName') {
            return error_maxlength(data, maxLengthNameSearch, inputLength);
        } else {
            return error_maxlength(data, maxLengthName, inputLength);
        }
    }
};
$.validator.addMethod("checkLengthUpdate", function (value, element) {
    if (value.length >= 8 && value.length <= 20) {
        return true;
    } else {
        return false;
    }
}, error_length_Update);

$.validator.addMethod("checkLengthRegister", function (value, element) {
    if (value.length >= 8 && value.length <= 20) {
        return true;
    } else {
        return false;
    }
}, error_length_Register);


$.validator.messages.equalTo = function (param, input) {
    $data = $(input).data('label');
    return error_equalTo($data);
};


$.validator.addMethod("checkByteEmailUpdate", function (value, element) {
    var pattern = /^[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~@.]*$/;
    return this.optional(element) || pattern.test(value);
}, error_email1ByteUpdate);

$.validator.messages.checkByteEmailUpdate = function (param, input) {
    $data = $(input).data('label');
    return error_email1ByteUpdate($data);
};

$.validator.messages.required = function (param, input) {
    $data = $(input).data('label');
    return require($data);
};

$.validator.addMethod("dateCheck", function (value, element) {
    if (value == "") {
        return true;
    }
    var date = value.split("/");
    var year = date[0];
    var month = date[1];
    var day = date[2];
    if (year.length != 4 || month.length != 2 || day.length != 2) {
        return false;
    }
    var date = new Date(year, month - 1, day);
    if (date.getFullYear() == year && date.getMonth() + 1 == month && date.getDate() == day) {
        return true;
    }
    return false;
}, error_date_format);

$.validator.messages.dateCheck = function (param, input) {
    $data = $(input).data('label');
    return error_date_format($data);
};

$.validator.addMethod("dateCompareFrom", function (value, element,params) {
        var enteredDateFrom = $('#enteredDateFrom').val();
        var enteredDateTo = $('#enteredDateTo').val();
        if(enteredDateFrom == "" || value == "" || enteredDateTo == "") {
            return true;
        }
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) <= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) 
            || (Number(value) <= Number($(params).val())); 
}, error_date);

$.validator.addMethod("dateCompareTo", function (value, element, params) {
        var enteredDateFrom = $('#enteredDateFrom').val();
        if(enteredDateFrom == "" || value == "") {
            return true;
        }
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) 
            || (Number(value) >= Number($(params).val())); 
    }, error_date);


$.validator.addMethod("regExpression", function (value, element) {
    return this.optional(element) || /^(?=.*[0-9])(?=.*[a-z])/.test(value);
}, error_regExpression);

$.validator.messages.regExpression = function (param, input) {
    $data = $(input).data('label');
    return error_regExpression($data);
};

$.validator.addMethod("checkByteWordUpdate", function (value, element) {
    //check 1 byte word
    var pattern = /^[a-zA-Z0-9\s.]*$/;
    return this.optional(element) || pattern.test(value);
}, error_byteWordUpdate);

$.validator.addMethod("checkByteWordRegister", function (value, element) {
    var pattern = /^[a-zA-Z0-9\s.]*$/;
    return this.optional(element) || pattern.test(value);
}, error_byteWordUpdate);

$.validator.addMethod("checkByteEmailRegister", function (value, element) {
    var pattern = /^[a-zA-Z0-9!#$%&'*+-/=?^_`{|}~@.]*$/;
    return this.optional(element) || pattern.test(value);
}, error_email1ByteRegister);

$.validator.messages.checkByteEmailRegister = function (param, input) {
    $data = $(input).data('label');
    return error_email1ByteRegister($data);
};

$.validator.messages.checkByteWordRegister = function (param, input) {
    $data = $(input).data('label');
    return error_byteWordUpdate($data);
};

$.validator.messages.checkByteWordUpdate = function (param, input) {
    $data = $(input).data('label');
    return error_byteWordUpdate($data);
};

$.validator.setDefaults({
    errorPlacement: function (error, element) {
        error.addClass('text-danger');
        error.insertAfter(element);
        error.css('color', 'red');
        if(element.attr('name') == 'enteredDateFrom') {
            error.appendTo('#enteredDateFromError');
        }
        if(element.attr('name') == 'enteredDateTo') {
            error.appendTo('#enteredDateToError');
        }
        if(element.attr('name') == 'username') {
            error.appendTo('#usernameError');
        }
        if (element.attr('name') == 'email') {
            error.appendTo('#emailError');
        }
        if (element.attr('name') == 'entered_date') {
            error.appendTo('#enteredDateError');
        }
        if (element.attr('name') == 'division' || element.attr('name') == 'division[]') {
            error.appendTo('#divisionError');
        }
        if (element.attr('name') == 'position' || element.attr('name') == 'position[]') {
            error.appendTo('#positionError');
        }
        if (element.attr('name') == 'password') {
            error.appendTo('#passwordError');
        }
        if (element.attr('name') == 'confirmPassword') {
            error.appendTo('#confirmPasswordError');
        }
    }
});