<?php

namespace App\Http\Controllers;

use App\Models\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Imports\DivisionImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class DivisionController extends Controller
{

    public function index()
    {
        $positionData = config('constant.positions');
        $position_id_logged = Auth::user()->position_id;
        if($position_id_logged != $positionData['id'][0]){
            Auth::logout();
            session()->forget('user');
            session()->flush();
            session()->invalidate();
            return redirect()->route('login.get');
        }
        $divisions = Division::orderBy('id', 'asc')->paginate(10);
        $positionData = config('constant.positions');
        return view('user.division', compact('divisions'))->with('positionData', $positionData);
    }

    public function import(Request $request)
    {   
        DB::beginTransaction();
        $errorsForReurnToBladeFile = [];
        //check format file to find row and column null, if null then return error
        $file = $request->file('file');

        $rowNumber = 0;
        $failures = [];
        $messageECL_CSV_001 = config('constant.messages.error.ECL_CSV_001');
        if (($handle = fopen($file, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                $rowNumber++;
                if (empty($row[0]) && empty($row[1]) && empty($row[2]) && empty($row[3]) && empty($row[4]) && empty($row[5]) && empty($row[6]))
                {
                    $errorsForReurnToBladeFile[] = [
                        'row' => 1,
                        'errors' => [$messageECL_CSV_001],
                    ];
                }  
                if (count($row) > 6) {
                    $errorsForReurnToBladeFile[] = [
                        'row' => 1,
                        'errors' => [$messageECL_CSV_001],
                    ];
                }
            }
            fclose($handle);
        }

        $headerRow = fopen($file, 'r');
        $header = fgetcsv($headerRow);
        $header = array_map('strtolower', $header);

        if (empty($header[0]) || empty($header[1]) || empty($header[2]) || empty($header[3]) || empty($header[4]) || empty($header[5])) {
            $errorsForReurnToBladeFile[] = [
                'row' => 1,
                'errors' => [$messageECL_CSV_001],
            ];
        }
        if (count($header) > 6) {
            $errorsForReurnToBladeFile[] = [
                'row' => 1,
                'errors' => [$messageECL_CSV_001],
            ];
        }
        if ($rowNumber == 1) {
            $errorsForReurnToBladeFile[] = [
                'row' => 1,
                'errors' => [$messageECL_CSV_001],
            ];
        }
        if (!empty($errorsForReurnToBladeFile)) {
            $errorsForReurnToBladeFile = array_map("unserialize", array_unique(array_map("serialize", $errorsForReurnToBladeFile)));
            Session()->flash('errors', $errorsForReurnToBladeFile);    
            return response()->json([
                'status' => 422,
                'success' => false,
                'errors' => $errorsForReurnToBladeFile,
            ]);
        } else {
            try {
                $data = $request->all();
                $file = $data['file'];
                $excel = \Maatwebsite\Excel\Facades\Excel::toArray(new DivisionImport, $file);
                $maxlength = config('constant.import.division.division_name.max_length');
                $messagesECL002a = config('constant.messages.error.ECL002.ECL002a');
                $messagesECL002b = config('constant.messages.error.ECL002.ECL002b');
                $messagesECL002c = config('constant.messages.error.ECL002.ECL002c');
                foreach ($excel[0] as $key => $value) {
                    if($value['division_name'] != NULL){
                        $rowname = 'Division Name';
                        $division_name = mb_strlen($value['division_name']);
                        if($division_name > $maxlength){
                            $flag = true;
                            $error_message = $rowname . $messagesECL002a . $maxlength . $messagesECL002b . $division_name . $messagesECL002c;
                            $errorsForReurnToBladeFile[] = [
                                'row' => $key + 2,
                                'attribute' => 'division_name',
                                'errors' => [$error_message],
                                'values' => 'division_name',
                            ];
                        }
                    }
                    
                }

                $import = new DivisionImport();
                $import->import($file);
                foreach ($import->failures() as $failure) {
                    if ($failure->row()) {
                        $flag = true;
                        $errorsForReurnToBladeFile[] = [
                            'row' => $failure->row(),
                            'attribute' => $failure->attribute(),
                            'errors' => $failure->errors(),
                            'values' => $failure->values(),
                        ];
                    }
                }
            }
            catch (\Illuminate\Database\QueryException $e) {
                $flag = true;
            } 
            catch (\Exception $e) {
                $flag = true;
                $messageECL_CSV_001 = config('constant.messages.error.ECL_CSV_001');
                $errorsForReurnToBladeFile = [];
                $errorsForReurnToBladeFile[] = [
                    'row' => 1,
                    'attribute' => 'file',
                    'errors' => [$messageECL_CSV_001],
                    'values' => 'file',
                ];
                $errorsForReurnToBladeFile = array_map("unserialize", array_unique(array_map("serialize", $errorsForReurnToBladeFile)));
            } finally {
                if(isset($flag)){
                    DB::rollBack();
                    if(count($errorsForReurnToBladeFile) > 0){
                        usort($errorsForReurnToBladeFile, function($a, $b) {
                            if ($a['row'] == $b['row']) {
                                return $a['attribute'] <=> $b['attribute'];
                            }
                            return $a['row'] <=> $b['row'];
                        });
                        Session()->flash('errors', $errorsForReurnToBladeFile);    
                    }
                    return response()->json([
                        'status' => 422,
                        'success' => false,
                        'errors' => $errorsForReurnToBladeFile,
                    ]); 
                } 
                DB::commit();
                return response()->json([
                    'status' => 200,
                    'success' => true,
                ]);
            }
        }
    }
}
