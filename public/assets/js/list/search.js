$("#enteredDateTo").on('change', function () {
    if ($(this).val() != "" && $("#enteredDateFrom").val() != "") {
        $("#enteredDateTo").rules("add", { dateCompareTo: "#enteredDateFrom" });
    }
    if($(this).val() != "" && $("#enteredDateFrom").val() == ""){
        $("#enteredDateTo").rules("remove", "dateCompareTo");
    }

    //valid form
    $("#listUser").valid();

});

//enteredDateFrom on change
$("#enteredDateFrom").on('change', function () {
    if ($(this).val() != "" && $("#enteredDateTo").val() != "") {
        $("#enteredDateTo").rules("add", { dateCompareTo: "#enteredDateFrom" });
    }
    if($(this).val() != "" && $("#enteredDateTo").val() == ""){
        $("#enteredDateTo").rules("remove", "dateCompareTo");
    }
    $("#listUser").valid();
});

//enteredDateTo on keyup
$("#enteredDateTo").on('keyup', function () {
    if ($(this).val() != "" && $("#enteredDateFrom").val() != "") {
        $("#enteredDateTo").rules("add", { dateCompareTo: "#enteredDateFrom" });
    } 
    if($(this).val() != "" && $("#enteredDateFrom").val() == ""){
        $("#enteredDateTo").rules("remove", "dateCompareTo");
    }
    $("#listUser").valid();
});

//enteredDateFrom on keyup
$("#enteredDateFrom").on('keyup', function () {
    if ($(this).val() != "" && $("#enteredDateTo").val() != "") {
        $("#enteredDateTo").rules("add", { dateCompareTo: "#enteredDateFrom" });
    }
    if($(this).val() != "" && $("#enteredDateTo").val() == ""){
        $("#enteredDateTo").rules("remove", "dateCompareTo");
    }
    $("#listUser").valid();
});

$('#listUser').validate({
    onkeyup: function (element) {
        $(element).valid();
    },
    rules: {
        username: {
            maxlength: 100,
        },
        enteredDateFrom: {
            dateCheck: true,
        },
        enteredDateTo: {
            dateCheck: true,
        },
    },
    submitHandler: function (form) {
        form.submit();
        $('input[type="submit"]').attr('disabled', 'disabled');
    },
});