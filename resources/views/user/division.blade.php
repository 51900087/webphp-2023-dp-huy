@extends('layout.admin')
@section('menu_division', 'active')
@section('title', 'Division List')
@section('sliderbar')
@section('admin_content')
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{asset('assets/js/common/errors.js')}}"></script>
<meta name="_token" content="{{ csrf_token() }}">
<div class="card">
	<div class="card-body">
        <style type="text/css">
            #right {
                margin-top: 20px;
                margin-bottom: 20px;
                display: flex;
                justify-content: right;
                align-items: right;
            }
        </style>
        {{-- @if (Session::has('errors'))
            <center id="right">
                {{$divisions->appends(['errors' => Session::get('errors')])->links()}}
            </center>
        @else --}}
            <center id="right">
                {{$divisions->links('vendor.pagination.bootstrap-4')}}
            </center>
        {{-- @endif --}}
	</div>
	<div class="card">
        <div class="card-body">
            <div class="alert alert-danger" id="error" style="display: none;">
                <ul></ul>
            </div>
            @if(Session::has('errors'))
                @php
                    $failures = Session::get('errors');
                @endphp
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($failures as $key => $failure)
                            @if($failure['row'] == 1)
                                <li>{{$failure['errors'][0]}}</li>
                            @else
                            <li>Dòng: {{$failure['row'] . $failure['errors'][0] }}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
		<div class="card-body">
            {{-- @if(!empty($divisions))
			<style type="text/css">
				#center {
					margin-top: 20px;
					margin-bottom: 20px;
					display: flex;
					justify-content: center;
					align-items: center;
				}
			</style> --}}
			@if($divisions->count() <= 0)
			<center id="center"  class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong> No Record</strong>
			</center>
			@else
			<table id="table" class="table table-responsive table-bordered table-striped table-hover">
				<thead>
					<tr>
					<th scope="col"><label>ID</label></th>
					<th scope="col"><label>Division Name</label></th>
                    <th scope="col"><label>Division Note</label></th>
                    <th scope="col"><label>Division Leader</label></th>
                    <th scope="col"><label>Floor Number</label></th>
					<th scope="col"><label>Created Date</label></th>
					<th scope="col"><label>Updated Date</label></th>
                    <th scope="col"><label>Deleted Date</label></th>
					</tr>
				</thead>
				<tbody>
                    @php 
                        $numberLimit = 30;
                    @endphp
						@foreach ($divisions as $division)
						<tr>
							<td>{{ $division->id }}</a></td>
							<td>{{ Illuminate\Support\Str::limit($division->name,  $numberLimit)}}</td>
							<td>{{ Illuminate\Support\Str::limit($division->note,  $numberLimit)}}</td>
                            @php 
                                $userModel = new App\Models\User();
                                $result = $userModel->getUsernameByDivisionLeaderID($division->division_leader_id);
                                $name = $result->name ?? '';

                            @endphp
							<td>{{  Illuminate\Support\Str::limit($name,  $numberLimit)}}</td>
                            <td>{{ $division->division_floor_num }}</td>
                            @php 
                                $division->created_date = date('Y/m/d', strtotime($division->created_date));
                                $division->updated_date = date('Y/m/d', strtotime($division->updated_date));
                                $division->deleted_date = date('Y/m/d', strtotime($division->deleted_date));
                            @endphp 
                            <td><label>{{ $division->created_date }}</label></td>
                            <td><label>{{ $division->updated_date ?? '' }}</label></td>
                            <td><label>{{ $division->deleted_date ?? '' }}</label></td>
						</tr>
						@endforeach
				</tbody>
			</table>
            @endif
		</div>
	</div>
    <div class="card-body">
        <div class="col-md-6">
            <div class="input-group mb-3">
                <div class="input-group mb-3">
                    <button type="button" name="btnImport" id="btnImport" class="btn btn-primary">Import CSV</button>
                    <input type="file" id="file" name="file" style="display: none;">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <script type="text/javascript" src="{{asset('assets/js/division/file.js')}}"></script> --}}
<script type="text/javascript">

    //function to render error
    // function renderError(errors){
    //     var html = '';
    //     $.each(errors, function(key, value){
    //         html += '<li>'+value+'</li>';
    //     });
    //     $('#error').show();
    //     $('#error').html(html);
    // }

	$('#btnImport').click(function(){
        $('#file').click();
	})
    $('#file').change(function(){
        var file_data = $('#file').prop('files')[0];
        if(file_data.type != 'text/csv'){
            swal({
                title: "エラー!",
                text: "ファイル形式が誤っています。CSVを選択してください。",
                icon: "error",
                button: "OK",
            })
            .then((value) => {
                $('#file').val('');
                location.reload();
            });
            return false;
        }
        //check file size must be <= 1MB
        if(file_data.size > 1048576){
            swal({
                title: "エラー!",
                text: "ファイルのサイズ制限１MBを超えています。",
                icon: "error",
                button: "OK",
            })
            .then((value) => {
                $('#file').val('');
                location.reload();
            });
            return false;
        }
        
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ route('division.import') }}",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){
                if(response.status == 200){
                    swal({
                        title: "成功!",
                        icon: "success",
                        button: "OK",
                    })
                    .then((value) => {
                        location.reload();
                    });
                }
                else{
                    swal({
                        title: "エラー!",
                        icon: "error",
                        button: "OK",
                    })
                    .then((value) => {
                        location.reload();
                    });
                }
            },
            error: function(data){
                swal({
                    title: "エラー!",
                    icon: "error",
                    button: "OK",
                }).then((value) => {
                    location.reload();
                });
            },
        })
    });
</script>
@endsection